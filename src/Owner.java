/**
 * Created by austinturner on 11/11/16.
 */
public class Owner {

    private String name;
    private String phone;

    public Owner(String phone, String name) {
        this.phone = phone;
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public String getPhone() {
        return phone;
    }

}
