/**
 * Created by austinturner on 11/11/16.
 */
public class Car {

    // class attributes
    private String color;
    private final String MAKE;

    // constructor
    public Car(String color) {
        this.color = color;
        MAKE = "FORD";
    }

    public Car(String color, String make) {
        this.color = color;
        this.MAKE = make;
    }

    // constructor
    public Car() {
        MAKE = "FORD";
    }



    //getter
    public String getColor() {
        return color;
    }

    public void setColor(String color) {

        this.color = color;
    }

    public String getMake() {
        return MAKE;
    }

    public String toString() {
        return "Your " + MAKE + " is " + color;
    }

}
