/**
 * Created by austinturner on 11/11/16.
 */
public class FirstClass {


    private static String name = "Austin";

    private static boolean status = false;
    private static int age = 12;
    private static long ageLong = 12;
    private static double height = 12.4;
    private static float something = 12.4F;

    private static Integer myIntObj = 0;


    public static void main(String[] args) {

        Car myCar1 = new Car();

        Car myCar2 = new Car("Red");
        Car myCar3 = new Car("Blue", "Chevy");
        System.out.println(myCar1);
        myCar1.setColor("BLACK");
        System.out.println(myCar1);
        hello(myCar1);

        System.out.println(myCar1);
        System.out.println(myCar2.toString());
        System.out.println(myCar3);
    }


    public static void hello(Car car) {
        car.setColor("GREEN");
    }


}


